/* eslint-disable no-continue */
export default class Biorobot {
  public coinCountHtml: HTMLElement = document.querySelector('.wallet__count');

  public creationInfo: HTMLElement = document.querySelector('.create__info');

  public creationInfoUpdate: HTMLElement = document.querySelector('.create__info-update');

  public addByCheckbox: HTMLInputElement = document.querySelector('#upBy');

  public robotImg: HTMLImageElement = document.querySelector('.create__img-group-warp img');

  public buyBtns: NodeList = document.querySelectorAll('.buy-item__button');

  public saleBtns: NodeList = document.querySelectorAll('.sale-item__button');

  public partsProd: NodeList = document.querySelectorAll('.create .part-checkbox__input');

  public radioProd: NodeList = document.querySelectorAll('.create .radio__input');

  public createButton: HTMLInputElement = document.querySelector('.create__button');

  private wordEnding: string;

  private partsList: { [key: string]: number } = { arm: 0, cpu: 0, soul: 0 };

  private partsInUse: { [key: string]: number } = { arm: 0, cpu: 0, soul: 0 };

  private productionAvailable: boolean = true;

  private curCoinCount: number = 40;

  // Start values
  readonly upByCount: number = 5;

  readonly partsPrice: { [key: string]: number } = { arm: 7, cpu: 5, soul: 25 };

  readonly partsSale: { [key: string]: number } = { arm: 5, cpu: 3, soul: 15 };

  readonly createPrice: number = 10;

  // Functions
  /**
   * Change image by selected options
   */
  imgChange(created: Boolean) {
    let type: string;
    let stab: string;
    let stage: string = '3';

    Array.prototype.forEach.call(this.radioProd, (el: HTMLInputElement) => {
      if (el.checked === true) {
        switch (el.getAttribute('name')) {
          case 'type':
            type = el.getAttribute('value');
            break;
          case 'stab':
            stab = el.getAttribute('value');
            break;
          default:
            break;
        }
      }
    });

    this.productionAvailable = true;
    Object.entries(this.partsList).forEach(
      ([key, value]) => {
        switch (key) {
          case 'soul':
            if (value < 1) this.productionAvailable = false;
            break;
          default:
            if (value < 4) this.productionAvailable = false;
            break;
        }
      },
    );

    if (this.createButton.disabled === false || this.productionAvailable) stage = '2';
    else if (created) stage = '1';

    this.robotImg.src = `/assets/img/@1x/${type}${stab}_${stage}.png`;
  }

  /**
   * Changed words ending by qunatity
   */
  declension(num: number, expressions: Array<string>) {
    let result;
    let count = num % 100;
    const [one, two, three] = expressions;

    if (count >= 5 && count <= 20) {
      result = three;
    } else {
      count %= 10;
      if (count === 1) {
        result = one;
      } else if (count >= 2 && count <= 4) {
        result = two;
      } else {
        result = three;
      }
    }

    this.wordEnding = result;
    return this.wordEnding;
  }

  /**
   * Check if robot can be created and update image
   */
  checkProduction() {
    let checkedCount: number = 0;
    const checkedObj: { [key: string]: number } = { arm: 4, cpu: 4, soul: 1 };

    Array.prototype.forEach.call(this.partsProd, (el: HTMLInputElement) => {
      if (el.checked) {
        const name: string = el.getAttribute('name').slice(0, -1);
        checkedObj[name] -= 1;
        checkedCount += 1;
      }
    });

    if (checkedCount < 9 || this.curCoinCount < 10) {
      let missingParts: string = '';
      this.createButton.disabled = true;

      if (this.curCoinCount < this.createPrice) {
        const coinNeed: number = this.createPrice - this.curCoinCount;
        missingParts = `${coinNeed} biorobo ${this.declension(coinNeed, ['монеты', 'монеты', 'монет'])}`;
      }

      Object.entries(checkedObj).forEach(
        ([key, value]) => {
          if (value > 0) {
            switch (key) {
              case 'arm':
                if (missingParts.length > 0) missingParts += ', ';
                missingParts += `${value} ${this.declension(value, ['биомеханизма', 'биомеханизма', 'биомеханизмов'])}`;
                break;
              case 'cpu':
                if (missingParts.length > 0) missingParts += ', ';
                missingParts += `${value} ${this.declension(value, ['процессора', 'процессора', 'процессоров'])}`;
                break;
              case 'soul':
                if (missingParts.length > 0) missingParts += ' и ';
                missingParts += ' души';
                break;
              default:
                break;
            }
          }
        },
      );

      this.creationInfoUpdate.innerHTML = missingParts;
      this.creationInfo.style.display = 'block';
    } else {
      this.createButton.disabled = false;
      this.creationInfo.style.display = 'none';
    }

    this.imgChange(false);
  }

  /**
   * Check if buy or sale buttons is available and change production block
   */
  checkButtons() {
    // Buy block
    Array.prototype.forEach.call(document.querySelectorAll('.buy__item'), (el: HTMLElement) => {
      const btnBuy: HTMLInputElement = el.querySelector('.button');
      const name = el.querySelector('.buy-item__name').getAttribute('data-name');
      const price: number = this.partsPrice[name];
      btnBuy.disabled = (this.curCoinCount < price);
    });

    // Sale block
    Array.prototype.forEach.call(document.querySelectorAll('.sale__item'), (el: HTMLElement) => {
      const name: string = el.querySelector('.sale-item__name').getAttribute('data-name');
      const btnBuy: HTMLInputElement = el.querySelector('.button');
      const partCurCount: HTMLElement = el.querySelector('.sale-item__count');
      const availableCount: number = this.partsList[name] - this.partsInUse[name];

      btnBuy.disabled = (availableCount === 0);
      partCurCount.innerHTML = String(availableCount);
    });


    // Production block
    Object.entries(this.partsList).forEach(
      ([name]) => {
        for (let index = 1; index <= 4; index += 1) {
          if (name === 'soul' && index > 1) {
            continue;
          }

          const partEl: HTMLInputElement = document.querySelector(`#${name}${index}`);
          if (index <= this.partsList[name]) {
            partEl.disabled = false;
          } else {
            partEl.disabled = true;
            partEl.checked = false;
          }
        }
      },
    );

    this.checkProduction();
  }

  /**
   * Change coin quantity and update wallet block
   */
  changeCoinQuantity(up: number) {
    this.curCoinCount += up;

    const coinWrap = document.querySelector('.wallet__coin-wrap');
    const lastCoin = 100 - this.curCoinCount;
    let newCoins = '';

    for (let index = 100; index > lastCoin; index -= 1) {
      newCoins += `<img src="/assets/img/@1x/coin.png" class="wallet__coin_img" alt="coin" style="z-index:${index}"></img>`;
    }

    if (this.curCoinCount > 100) {
      this.curCoinCount = 100;
      document.getElementById('modal_max').style.display = 'block';
    }

    coinWrap.innerHTML = newCoins;
    this.coinCountHtml.innerHTML = String(this.curCoinCount);

    this.checkButtons();
  }

  /**
   * Buy or sale parts and update global vars
   */
  partsUpdate(name: string, price: number) {
    if (price > 0) {
      this.partsList[name] -= 1;
    } else {
      this.partsList[name] += 1;
    }

    this.changeCoinQuantity(price);
  }

  // events
  eventAddCoin() {
    let up: number = 1;
    if (this.addByCheckbox.checked === true) {
      up = this.upByCount;
    }

    this.changeCoinQuantity(up);
  }

  eventBuyPart(name: string) {
    this.partsUpdate(name, this.partsSale[name] * -1);
  }

  eventSalePart(name: string) {
    this.partsUpdate(name, this.partsSale[name]);
  }

  eventPartSelected(name: string, checked: boolean) {
    if (checked) {
      this.partsInUse[name] += 1;
    } else {
      this.partsInUse[name] -= 1;
    }

    this.checkButtons();
  }

  eventCreateRobot() {
    document.getElementById('modal_success').style.display = 'block';

    this.partsList.arm -= 4;
    this.partsList.cpu -= 4;
    this.partsList.soul -= 1;

    this.partsInUse = { arm: 0, cpu: 0, soul: 0 };

    Array.prototype.forEach.call(this.partsProd, (el: HTMLInputElement) => {
      // eslint-disable-next-line no-param-reassign
      el.checked = false;
    });

    this.changeCoinQuantity(-this.createPrice);
    this.checkButtons();
    this.imgChange(true);
  }

  eventStart() {
    document.querySelector('.wallet__checkbox .checkbox__label').innerHTML = `Цыганить по ${this.upByCount} монет`;
    this.createButton.innerHTML = `Произвести за ${this.createPrice} монет`;
    this.changeCoinQuantity(0);

    Array.prototype.forEach.call(this.buyBtns, (el: HTMLElement) => {
      const block: HTMLElement = el.parentElement;
      const name: string = block.querySelector('.buy-item__name').getAttribute('data-name');
      const price: number = this.partsPrice[name];
      const title: HTMLElement = block.querySelector('.buy-item__price');

      title.innerHTML = `Стоимость: ${price} монет`;
    });

    Array.prototype.forEach.call(this.saleBtns, (el: HTMLElement) => {
      const block: HTMLElement = el.parentElement;
      const name: string = block.querySelector('.sale-item__name').getAttribute('data-name');
      const price: number = this.partsSale[name];
      const title: HTMLElement = block.querySelector('.sale-item__price');

      title.innerHTML = `Стоимость: ${price} монет`;
    });

    this.checkProduction();
  }
}
