import Biorobot from './BiorobotClass.ts';

const BiorobotClass = new Biorobot();
/**
 * Scroll to bottom (click)
 */
document.querySelector('.header__button').addEventListener('click', () => {
  document.querySelector('.create').scrollIntoView({ behavior: 'smooth' });
});

/**
 * Add coin on click by 'naciganit' (click)
 */
document.querySelector('#add-coins').addEventListener('click', () => {
  BiorobotClass.eventAddCoin();
});

/**
 * Buy parts (click)
 */
Array.prototype.forEach.call(BiorobotClass.buyBtns, (el: HTMLElement) => {
  el.addEventListener('click', (event) => {
    const block = (<HTMLElement>(<HTMLElement>event.target).parentNode);
    const name = block.querySelector('.buy-item__name').getAttribute('data-name');
    BiorobotClass.eventBuyPart(name);
  });
});

/**
 * Sale parts (click)
 */
Array.prototype.forEach.call(BiorobotClass.saleBtns, (el: HTMLElement) => {
  el.addEventListener('click', (event) => {
    const block = (<HTMLElement>(<HTMLElement>event.target).parentNode);
    const name = block.querySelector('.sale-item__name').getAttribute('data-name');
    BiorobotClass.eventSalePart(name);
  });
});

/**
 * Check available production by parts (click)
 */
Array.prototype.forEach.call(BiorobotClass.partsProd, (el: HTMLElement) => {
  el.addEventListener('click', (event) => {
    const part = <HTMLInputElement>event.target;
    const name: string = part.getAttribute('name').slice(0, -1);

    BiorobotClass.eventPartSelected(name, part.checked);
  });
});

/**
 * Change image on option changed
 */
Array.prototype.forEach.call(BiorobotClass.radioProd, (el: HTMLElement) => {
  const radio = el;
  radio.onclick = () => BiorobotClass.imgChange(false);
});

/**
 * Create robot event
 */
BiorobotClass.createButton.addEventListener('click', () => {
  BiorobotClass.eventCreateRobot();
});

document.addEventListener('DOMContentLoaded', () => {
  BiorobotClass.eventStart();
});
